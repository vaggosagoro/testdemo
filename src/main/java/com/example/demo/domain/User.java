package com.example.demo.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "User", uniqueConstraints = {@UniqueConstraint(columnNames = {"email", "ipAddress"})})
public class User {

    private static final int MAX_NAME_LENGTH = 30;
    private static final int MAX_MAIL_LENGTH = 50;
    private static final int MAX_IP_ADDRESS_LENGTH = 15;

    @Id
    @Column(name = "user_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull(message = "Fill the empty field")
    @Column(name = "firstname", nullable = false, length = MAX_NAME_LENGTH)
    private String firstname;

    @NotNull(message = "Fill the empty field")
    @Column(name = "lastname", nullable = false, length = MAX_NAME_LENGTH)
    private String lastname;

    @NotNull(message = "Fill the empty field")
    @Column(name = "email", nullable = false, length = MAX_MAIL_LENGTH)
    private String email;

    @NotNull(message = "Fill the empty field")
    @Column(name = "gender")
    private String gender;

    @NotNull(message = "Fill the empty field")
    @Column(name = "ipAddress", nullable = false, length = MAX_IP_ADDRESS_LENGTH)
    private String ipAddress;

    @Override
    public String toString() {
        return "User{" +
                "user_id=" + id +
                ", firstname=" + firstname +
                ", lastname=" + lastname +
                ", email='" + email + '\'' +
                ", gender=" + gender +
                ", ipAddress=" + ipAddress +
                '}';
    }

    public User(){

    }

    public User(String firstname, String lastname, String email, String gender, String ipAddress) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gender = gender;
        this.ipAddress = ipAddress;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
