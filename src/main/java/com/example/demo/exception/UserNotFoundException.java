package com.example.demo.exception;

public class UserNotFoundException extends Exception {

    public String errorMessage;

    public String getErrorMessage(){
        return errorMessage;
    }

    public UserNotFoundException(String errorMessage){
        super(errorMessage);
        this.errorMessage = errorMessage;
    }

    public UserNotFoundException() {
        super();
    }

}
