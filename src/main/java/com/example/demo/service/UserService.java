package com.example.demo.service;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List <User> findByEmail(String email){
        return userRepository.findByEmail(email);
    }

    public List<User> findByIpAddress(String ipAddress){
        return userRepository.findByIpAddress(ipAddress);
    }

    public User create(User user){
        return userRepository.save(user);
    }

}
