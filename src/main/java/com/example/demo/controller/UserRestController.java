package com.example.demo.controller;


import com.example.demo.domain.User;
import com.example.demo.error.ErrorResponse;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping(path = "/getUser")
    public List<User> findByEmail(@RequestParam String email)throws UserNotFoundException{

        if (!email.matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$")) {
            throw new UserNotFoundException("Invalid email format requested");
        }

        List<User> byEmail = userService.findByEmail(email);
        if (byEmail.isEmpty()) throw new UserNotFoundException("No user with email "+email);
        return byEmail;
    }

    @GetMapping(path = "/getUser/{ipAddress}")
    public List<User> findByIpAddress(@PathVariable String ipAddress) throws UserNotFoundException{

        if (!ipAddress.matches("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$")) {
            throw new UserNotFoundException("Invalid ip Address format requested");
        }

        List<User> byIpAddress = userService.findByIpAddress(ipAddress);
        if (byIpAddress.isEmpty()) throw new UserNotFoundException("No user with ip Address "+ipAddress);
        return byIpAddress;
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
        ErrorResponse error = new ErrorResponse();
        error.setErrorCode(HttpStatus.PRECONDITION_FAILED.value());
        error.setMessage(ex.getMessage());
        return new ResponseEntity<ErrorResponse>(error, HttpStatus.OK);
    }

    @PostMapping(path = "/saveUser")
    public User create (@Valid @RequestBody User user) throws UserNotFoundException{

        if (user.getFirstname().isEmpty() ||!user.getFirstname().matches("^[a-zA-Z]*$")){

            throw new UserNotFoundException ("Firstname field is invalid or empty");
        }

        if (user.getLastname().isEmpty() || !user.getLastname().matches("^[a-zA-Z]*$")){
            throw new UserNotFoundException ("Lastname field is invalid or empty");
        }

        if (user.getEmail().isEmpty() || !user.getEmail().matches("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{1,63}$")){
            throw new UserNotFoundException("Email field is invalid or empty. Example of a valid email: user@gmail.com");
        }

        if (user.getGender().isEmpty() || (!user.getGender().equals("MALE") && !user.getGender().equals("FEMALE"))){
            throw new UserNotFoundException("Gender should be FEMALE or MALE");
        }

        if (user.getIpAddress().isEmpty() || !user.getIpAddress().matches("^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$")){
            throw new UserNotFoundException("Ip Address is invalid or empty. Example of a valid ip address:127.0.0.1");
        }

        return userService.create(user);
    }

}
